import React, { Component } from 'react'
import { register } from './UserFunctions'

class Register extends Component {
    constructor() {
        super()
        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            password: ''
        }

        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    onChange(e) {
        this.setState({[e.target.name]: e.target.value })
    }
    onSubmit(e) {
        e.preventDefault()
        document.getElementById('check_text').style='display:none';
        if(this.state.first_name == ""){
            document.getElementById('check_text').style='display:block';
            document.getElementById('check_text').textContent='請輸入姓氏';
        }else if(this.state.last_name == ""){
            document.getElementById('check_text').style='display:block';
            document.getElementById('check_text').textContent='請輸入名字';
        }else if(this.state.email == ""){
            document.getElementById('check_text').style='display:block';
            document.getElementById('check_text').textContent='請輸入帳號';
        }else if(this.state.password == ""){
            document.getElementById('check_text').style='display:block';
            document.getElementById('check_text').textContent='請輸入密碼';
        }else{
            
        }
        const user = {
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            email: this.state.email,
            password: this.state.password
        }
        register(user).then(res => {   
            if(res){
                this.props.history.push(`/login`) 
            }else{
                document.getElementById('check_text').style='display:block';
                document.getElementById('check_text').textContent='帳號已存在';
            }       
        })
        
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-6 mt-5 mx-auto">
                        <form noValidate onSubmit={this.onSubmit}>
                            <h1 className="h3 mb-3 font-weight-normal">註冊</h1>
                            <div className="form-group">
                                <label htmlFor="first_name">姓氏</label>
                                <input type="text"
                                className="form-control"
                                name="first_name"
                                placeholder="Enter First Name"
                                value={this.state.first_name}
                                onChange={this.onChange}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="last_name">名字</label>
                                <input type="text"
                                className="form-control"
                                name="last_name"
                                placeholder="Enter Last Name"
                                value={this.state.last_name}
                                onChange={this.onChange}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="email">帳號</label>
                                <input type="email"
                                className="form-control"
                                name="email"
                                placeholder="Enter Email"
                                value={this.state.email}
                                onChange={this.onChange}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">密碼</label>
                                <input type="password"
                                className="form-control"
                                name="password"
                                placeholder="Enter Password"
                                value={this.state.password}
                                onChange={this.onChange}/>
                            </div>
                            <div id='check_text'></div>
                            <button type="submit"
                            className="btn btn-lg btn-primary btn-block">
                                Register
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default Register